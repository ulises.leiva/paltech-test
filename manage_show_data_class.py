import os
import json
import matplotlib.pyplot as plt
import numpy as np
import random
import shutil
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split



class manage_show_data_class:
    def __init__(self, dataset_folder_location, predict_dataset_folder_location, min_percentage_peak = 1.25, percentage_peak = 1.8, min_len_noise = 20, max_len_noise = 50, sequence_len = 50):
        #Set attributes.
        self.percentage_peak_with_a_rock = percentage_peak
        self.min_len_noise = min_len_noise
        self.max_len_noise = max_len_noise
        self.min_percentage_peak = min_percentage_peak
        #Set paths for datasets.
        if dataset_folder_location != '':
            json_files = [file for file in os.listdir(dataset_folder_location) if file.endswith('.json')]
            self.json_paths = [os.path.join(dataset_folder_location, file) for file in json_files]
        if predict_dataset_folder_location != '':
            json_files = [file for file in os.listdir(predict_dataset_folder_location) if file.endswith('.json')]
            self.json_paths_predict = [os.path.join(predict_dataset_folder_location, file) for file in json_files]    
        self.sequence_len = sequence_len
        
    # Function for show the JSON data (Current and RPM).
    def show_json_data(self, n):
        # Catch if the N is bigger than dataset.
        if n > len(self.json_paths):
            raise ValueError("Number bigger than data.")

        # Load and plot data.
        with open(self.json_paths[n], 'r') as f:
            file_content = f.read()
            try:
                data = json.loads(file_content)
            except json.JSONDecodeError as e:
                print(f"Error decoding JSON: {e.msg}")
                return
            current_data = data['current']
            rpm_data = data['rpm']

            plt.figure(figsize=(10, 10))
            plt.subplot(2, 1, 1)
            x = np.arange(len(current_data))
            plt.plot(x, current_data, label='Current [A]', color='b')
            plt.xlabel('Time')
            plt.ylabel('Current [A]')
            plt.title('Current Graphic')
            plt.legend()

            plt.subplot(2, 1, 2)
            x = np.arange(len(rpm_data))
            plt.plot(x, rpm_data, label='RPM', color='r')
            plt.xlabel('Time')
            plt.ylabel('RPM')
            plt.title('RPM Graphic')
            plt.legend()
            
            plt.tight_layout()
            plt.show()

    # This function filter the emptys JSONs.
    def filter_empty_json_data(self):
        deleted_jsons = []
        for path in self.json_paths:
            if os.path.getsize(path) == 0:  
                deleted_jsons.append(path)
                os.remove(path)  
                self.json_paths.remove(path)
        print("deleted files:", deleted_jsons)
        return self.json_paths

    # This function filter the extractions with length < 0.8 * mean(length_all_data)
    def filter_short_extractions(self):
        current_length = []
        # Get the length of current from JSONs
        for index in range(len(self.json_paths)):
            with open(self.json_paths[index], 'r') as f:
                file_content = f.read()
                try:
                    data = json.loads(file_content)
                    current_length.append(len(data['current']))
                except json.JSONDecodeError as e:
                    print(f"Error decoding JSON: {e.msg}")
                    return
        # Calculate the mean of the length from all the current datas and remove de datas smaller than the threshold.
        current_length_mean = np.mean(current_length)
        threshold = 0.8*current_length_mean
        paths_to_remove = []
        for index in range(len(self.json_paths)):
            if current_length[index] < threshold:
                paths_to_remove.append(self.json_paths[index])
        for path in paths_to_remove:
            os.remove(path) 
            self.json_paths.remove(path)
        return self.json_paths

    # Move a file to the input path
    def move_file_to_folder(self, file_path, folder_path):
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)

        file_name = os.path.basename(file_path)
        full_folder_path = os.path.join(folder_path, file_name)
        shutil.move(file_path, full_folder_path)

    # This function returns de train and predict dataset.
    def create_dataset(self):
        dataset = []
        # separate train dataset on windows of length = (sequence_len).
        # excess data is removed.
        for index in range(len(self.json_paths)):
            with open(self.json_paths[index], 'r') as f:
                file_content = f.read()
                try:
                    data = json.loads(file_content)
                    current_length = len(data['current'])
                    module = current_length //self.sequence_len
                    dataset.append(data['current'][0:module*self.sequence_len])
                except json.JSONDecodeError as e:
                    print(f"Error decoding JSON: {e.msg}")
                    return
        # The dataset is shuffle, the peak from all datasets is calculated and the dataset is separated in two (with rock and without rock).
        random.shuffle(dataset)
        self.peak = max(max(sublist) for sublist in dataset)
        length_each_dataset = int(len(dataset)/2)
        dataset_without_rock = dataset[0:length_each_dataset]
        dataset_with_rocks = dataset[length_each_dataset:]
        # Call to the function that add perturbation to the dataset with rock.
        dataset_with_rocks, _ = self.add_perturbation_to_dataset(dataset_with_rocks)
        # Call to the function that generate the sequences (input from the model).
        predictors_without_rock = self.generate_sequences(dataset_without_rock, start=0)
        predictors_with_rock = self.generate_sequences(dataset_with_rocks, start=0)
        # Call to the function that create and generate the sequence for the dataset for the predict.
        predict_dataset, Y_to_predict, when_te_perturbation_start = self.create_predict_dataset()
        predictors_to_predict, number_sequence_by_prediction = self.generate_sequences_prediction(predict_dataset, 0)
        # Create the outputs for the model (Y).
        Y_without_rock = np.zeros(len(predictors_without_rock))
        Y_with_rock = np.ones(len(predictors_with_rock))
        # Concatenate all the data and scale it.
        predictors = np.concatenate((predictors_without_rock, predictors_with_rock, predictors_to_predict)) 
        predictors = self.scale_x_data(predictors)
        # Separathe the data in prediction and training dataset
        len_predictors = len(predictors_without_rock)+ len(predictors_with_rock)
        predictors_to_predict = predictors[len_predictors:, :]
        predictors = predictors[:len_predictors, :]
        Y = np.concatenate((Y_without_rock, Y_with_rock))
        return predictors, Y, predictors_to_predict, Y_to_predict, number_sequence_by_prediction, when_te_perturbation_start

    # Function that add perturbation for simulate the peaks of current in presence of rocks.
    # Mode = 0 is for training/test dataset.
    # Mode = 1 is for predict dataset.
    def add_perturbation_to_dataset(self, dataset_to_add_perturbation, mode = 0):
        when_te_perturbation_start = []
        for i, dataset in enumerate(dataset_to_add_perturbation):
            # Reset the index for iterate inside the dataset.
            # If the dataset is for training/test start the perturbation at the value = 0.
            # If not start the perturbation in a random number between te size of the windows and 3*size of the windows.  
            if mode == 0:
                index = 0
            else:
                index =  random.randint(self.sequence_len + 1, 3*self.sequence_len)
            when_te_perturbation_start.append(index)
            # While true for add the dataset progressively.
            while(1):
                # Set a number of peak random beetween the min_percentage and the max_percentage of peak.
                peak_with_a_rock = random.uniform(self.min_percentage_peak*self.peak, self.percentage_peak_with_a_rock*self.peak)
                # Set a random length of noise
                len_noise = random.randint(self.min_len_noise, self.max_len_noise)
                # Build the noise.
                if (index + len_noise >= len(dataset)):
                    substraction = peak_with_a_rock - np.max(dataset[index:len(dataset)])
                else:
                    substraction = peak_with_a_rock - np.max(dataset[index:index + len_noise])
                mean = substraction*0.75
                de = 0.25*substraction / (2 * 1.96)
                gaussian_noise = np.random.normal(mean, de, len(dataset))
                # In case that the length of the noise plus the current index of dataset is bigger than the length of dataset.
                # Add the noise until the length of the dataset.
                # If the value of the current is lower than the minimun value with perturbation put a random value for the current.
                if (index + len_noise >= len(dataset)):
                    dataset_to_add_perturbation[i] = [x + abs(y) if idx >= index and idx < len(dataset) else x for idx, (x, y) in enumerate(zip(dataset_to_add_perturbation[i], gaussian_noise))]
                    # for ind in range(index, len(dataset) - 1):
                    #     if (dataset_to_add_perturbation[i][ind]) < self.min_percentage_peak*self.peak:
                    #         dataset_to_add_perturbation[i][ind] += random.uniform(self.min_percentage_peak*self.peak - dataset_to_add_perturbation[i][ind], self.percentage_peak_with_a_rock*self.peak - dataset_to_add_perturbation[i][ind])
                # Else add the noise (all the length of the noise).
                # If the value of the current is lower than the minimun value with perturbation put a random value for the current.
                else:
                    dataset_to_add_perturbation[i] = [x + abs(y) if idx >= index and idx < index + len_noise else x for idx, (x, y) in enumerate(zip(dataset_to_add_perturbation[i], gaussian_noise))]
                    # for ind in range(index, index + len_noise):
                    #     if (dataset_to_add_perturbation[i][ind]) < self.min_percentage_peak*self.peak:
                    #         dataset_to_add_perturbation[i][ind] += random.uniform(self.min_percentage_peak*self.peak - dataset_to_add_perturbation[i][ind], self.percentage_peak_with_a_rock*self.peak - dataset_to_add_perturbation[i][ind])
                # Increase the index accordingly 
                if len_noise >= self.sequence_len:
                    index += len_noise 
                else:
                    index += self.sequence_len
                if (index >= len(dataset)):
                    break
        return dataset_to_add_perturbation, when_te_perturbation_start

    def generate_sequences(self, dataset, start):
        predictors = []
        # Iterate in the dataset.
        for data in dataset:
            # Iterate in each value of the dataset.
            for index in range(start, len(data) - self.sequence_len):
                # If the index is lower than the size of the windows
                # Replicats leftmost value.
                if index < self.sequence_len - 1:
                    sequence = [data[start]] * (self.sequence_len - index - 1)
                    sequence.extend(data[start:index + 1])
                # Else get the values for the windows.
                else:
                    sequence = data[index:self.sequence_len + index]
                predictors.append(sequence)
        predictors = np.array(predictors)
        return predictors

    # Scale de data for use the input of the model.
    def scale_x_data(self, predictors):
        scaler = MinMaxScaler(feature_range=(0, 1))
        scaler.fit(predictors)
        predictors = scaler.transform(predictors)
        return predictors
    
    # Split the dataset in training and test.
    def split_datasets(self, predictors, Y):
        x_train, x_test, y_train, y_test = train_test_split(predictors,Y, test_size=0.20, random_state=1)
        return x_train, x_test, y_train, y_test
    
    # Create the dataset for prediction.
    def create_predict_dataset(self):
        dataset = []
        # Load the prediction dataset.
        for index in range(len(self.json_paths_predict)):
            with open(self.json_paths_predict[index], 'r') as f:
                file_content = f.read()
                try:
                    data = json.loads(file_content)
                    dataset.append(data['current'])
                except json.JSONDecodeError as e:
                    print(f"Error decoding JSON: {e.msg}")
                    return
        # Shuffle the dataset and separate the dataset (with and without rocks).
        random.shuffle(dataset)
        length_each_dataset = int(len(dataset)/2)
        dataset_without_rock = dataset[0:length_each_dataset]
        dataset_with_rocks = dataset[length_each_dataset:]
        # Call to the function that add perturbation.
        dataset_with_rocks, when_te_perturbation_start = self.add_perturbation_to_dataset(dataset_with_rocks, mode=1)
        # Create the output (Y).
        Y_without_rock = np.zeros(len(dataset_without_rock))
        Y_with_rock = np.ones(len(dataset_with_rocks))
        dataset = dataset_without_rock + dataset_with_rocks
        Y = np.concatenate((Y_without_rock, Y_with_rock))
        return dataset, Y, when_te_perturbation_start
    
    # Generate the sequence for the prediction dataset.
    # Returns a list with the number of sequences by prediction.
    def generate_sequences_prediction(self, dataset, start):
        predictors = []
        number_sequence_by_prediction = []
        count = 0
        # Iterates in the dataset
        for data in dataset:
            # Iterates in each value of the dataset.
            for index in range(start, len(data) - self.sequence_len):
                # If the index of the value is lower than the size of the windows.
                # Repeat the leftmost value.
                if index < self.sequence_len - 1:
                    sequence = [data[start]] * (self.sequence_len - index - 1)
                    sequence.extend(data[start:index + 1])
                # Else the sequence is the length of the windows.
                else:
                    sequence = data[index - self.sequence_len + 1:index + 1]
                predictors.append(sequence)
            # Append the number of the sequence for the data.
            number_sequence_by_prediction.append(len(data) - self.sequence_len)
        return predictors, number_sequence_by_prediction
    

