# Modelo de detección de colisiones con rocas.

## Dependencias utilizadas

- Keras y Tensorflow: Utilizados en la construcción, entrenamiento y predicciones del modelo.
- Scikit-learn: Escalamiento de datos y para gráficar la matriz de confusión.
- matplotlib: Graficos.
- ipykernel: Trabajar con jupyter notebook.
- Numpy: Manejo de tensores.
## Instalación

Se cuenta con el archivo con las dependencias utilizadas en el ambiente trabajado. Para su instalación utilizar el siguiente comando:

`pip install -r requeriments_paltech_test.txt`

## Documentación y resultados.

Link del archivo: [Abrir PDF](./documentacion.pdf)

