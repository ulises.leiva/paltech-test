
from keras.models import Sequential
from keras.layers import Embedding,Dense,LSTM,Dropout,Flatten,BatchNormalization,Conv1D,GlobalMaxPooling1D,MaxPooling1D
from keras.optimizers import Adam
from manage_show_data_class import manage_show_data_class as ManageData
from sklearn.metrics import confusion_matrix
import numpy as np
from sklearn import metrics
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow import keras

class lstm_class:
    def __init__(self, sequence_len = 50):
    # Set the value of the length of the sequence
        self.sequence_len = sequence_len
    # Build the model
    def build_model(self):
        opt = Adam(learning_rate=0.01)
        self.model = Sequential()
        self.model.add(LSTM(50, input_shape=(self.sequence_len, 1)))
        #self.model.add(Dense(6))
        self.model.add(Dropout(0.7))
        self.model.add(Dense(1, activation='sigmoid'))
        self.model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])

    # Train the model
    def train_model(self, x_train, x_test, y_train, y_test):
        history_lstm = self.model.fit(x_train[:,:,np.newaxis], y_train, epochs=15,batch_size=64,validation_data=(x_test[:,:,np.newaxis],y_test),shuffle=True)
        return history_lstm

    # Predict with the model.
    # Also returns when the presence of rock is detected.
    def predict(self, sequences_for_predict, number_sequence_by_prediction):
        Y_predicted = []
        prefix = 0
        when_the_rock_is_detected = []
        # Iterates in the list that have the number of sequences by data.
        for number_sequence in number_sequence_by_prediction:
            out = 0
            rock_detected_in_sample = -1
            # Iterates in the sequences.
            for index in range(number_sequence):
                # Get the sequences, reshape and predict.
                sequence = np.array(sequences_for_predict[index + prefix])
                sequence = sequence.reshape(1,self.sequence_len,1)
                Y_pred = self.model.predict(sequence)
                # If detect a rock the out is 1. Get the index for the list: when the rock is detected. And break the for.
                if Y_pred[0, 0] >= 0.5:
                    out = 1
                    rock_detected_in_sample = index
                    break
            Y_predicted.append(out)
            prefix += number_sequence
            when_the_rock_is_detected.append(rock_detected_in_sample)
        return Y_predicted, when_the_rock_is_detected
    
    # Plot the confusion matrix.
    def plot_confusion_matrix(self, y_reference, y_predicted):
        confusion_matrix = metrics.confusion_matrix(y_reference, y_predicted)
        cm_display = metrics.ConfusionMatrixDisplay(confusion_matrix = confusion_matrix, display_labels = [False, True])
        cm_display.plot()
        plt.show()

    # Save the model.
    def save_model(self):
        self.model.save("model.h5")

    # Load the model.
    def load_model(self):
        self.model = keras.models.load_model("nombre_del_archivo.h5")